# MEAN - 01
> Práctica de Node.Js 
> API Rest desarrollada en Node y consumida con Angular 5

## Screenshots / Capturas de Pantalla
![enter image description here](https://lh3.googleusercontent.com/WYWAXaRS_HCKvwDEVyo0qR96K4gaDVQbJ15VEk0qdULPrdykMhvIkWbv9VpB0rwh9cckTdAn9GVh)

## Tech-framework used / Tecnologías Usadas
- Node
	- express
	- mongojs
	- cors
	- path
- Angular
- Mongodb
- Bootstrap
- Font-awesome

## Install / Instalación
#### OS X, Linux y Windows
*Abra un terminal y ejecute:*
```Shell
git clone http://gitlab.com/pulidovpe/login-node.git

cd login-node

npm install

node server.js
```
## Tasks / Lista de Tareas
- [x] Inicializar repositorio
- [x] Subir mis primeros cambios a GitLab
- [x] Completar el back-end
- [x] Probar el API
- [x] Completar el front-end

## Contribute / Para contribuir
1. Has un [Fork](https://gitlab.com/pulidovpe/login-node/forks/new)
2. Crea tu propia rama (git checkout -b feature/fooBar)
3. Sube tus cambios (git commit -am 'Add some fooBar')
4. Actualiza tu rama (git push origin feature/fooBar)
5. Has un "Pull Request"

## Credits / Créditos
En este proyecto, me he guiado del tutorial publicado en esta página:
[faztweb](http://www.faztweb.com/tutorial/crud-mean-angular-5), propiedad del usuario @github/FaztWeb

## License / Licencia
Pulido V.P.E. – @gitlab/pulidovpe – pulidovpe.dev@gmail.com
Distributed under the MIT license. See [LICENSE](LICENSE) for more information.